Scripting
=========

The Interpreter Window
----------------------

The simplest interface to FrIDa scripting is the dock window on the
bottom. It allows you to enter arbitrary GUILE commands and access the
FrIDa API. However it is not the preferred environment to do larger
scripting work in but rather a way to give a quick call start some of
the magic scripts.

Loading Scripts
---------------

If you have written a FrIDa script, you can evaluate it using the
Interpreter Menu. GUILE Scripts are assumend to end in .scm. There is
unrestricted GUILE access available as well as the FrIDa API.

Connect with geiser
-------------------

When the GUILE plugin is loaded, it automatically starts a geiser
server listening on a UNIX domain socket located in
``$XDG_RUNTIME_DIR`` if available, otherwise ``/tmp/runtime-$USER/``
or, as last resort, ``/tmp/frida-*/``. This allows you to connect
to FrIDa from your emacs. It gives you a powerfull REPL and full
programming support -- code completion, directly testing your commands
on a live binary.
