.. frida documentation master file, created by
   sphinx-quickstart on Mon Feb 16 11:43:07 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to frida's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   installing
   using			  
   scripting
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

