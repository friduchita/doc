Introduction
============

What is FrIDA?
--------------

Frida is a tool to better understand how some binary program works --
an Interactive Disassembler. Instead of the long instruction listing
you get by running objdump, FrIDa provides you with a control flow
graph and lets you annotate parts of the program you already
understood. It lets you add names to things (like functions) to
actually describe their function -- even if the binary has been
stripped and the original names are gone.

Why use FrIDa?
--------------

The competition in this field isn't particularly large and FrIDa may
well be the only tool available that works for you and fits your
wallet. FrIDa uses the LLVM library and, as a result, can handle
almost any binary program you encounter. It can do ELF binaries just
as well as PE (Windows) and Mac binaries for every CPU architecture
LLVM supports -- including x86 and ARM but also less common targets
like Power, SPARC or MIPS.


FrIDa is Free Software
----------------------

FrIDa is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
