Installation
------------

These instructions have all been tested on Debian jessie systems
only. Patches and hints for other distributions are welcome!

Build requirements
==================

Frida requires the following packages for building

* cmake
* pkg-config
* LLVM 3.5
* Qt5 (5.2 or newer preferred)
* QuaZIP
* log4cxx
* guile-2.0
* google-mock
* swig
* Boost (signals2)
* libedit (through LLVM)

On a debian (jessie) system this works with the following command:

.. code-block:: bash

   apt install cmake pkg-config llvm-3.5-dev libquazip-qt5-dev liblog4cxx10-dev qtbase5-dev guile-2.0-dev google-mock swig libboost-dev libedit-dev


Note that version 3.5 of LLVM is required.

Building
========

I would recommend an out-of tree build which works as follows

.. code-block:: bash

   mkdir build
   cd build
   cmake -DCMAKE_BUILD_TYPE=Release ..
   make -j

Which should generate you a frida binary. If you don't want to install
gmoch/gtest you need to remove the stanzas from CMakeList and not run
the tests.

Running
=======

If you want to run frida from the build directory instead of
installing it, you need to export ``LD_LIBRARY_PATH`` pointing to
your build directory so frida can find it's plugins and shared
libraries. Run it like

.. code-block:: bash

   export LD_LIBRARY_PATH=`pwd`/build
   build/frida
