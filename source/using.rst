Usage
-----

Adding and renaming functions
=============================

Functions can be added by entering an address -- this functionality is
available from the context-menu on the function-list widget as well as
the f hot-key. Frida will the recursively disassembler all functions
directly reachable from this entry point.

To rename the currently selected function use the r Hot-key. You can
also rename functions from the context-menu of the individual items in
the function list (by default located on the left side).

Adding comments
===============

You have the choice of adding "local" and "global" comments. Global
comments add information intended for human observers to an address
and are displayed at that address. Local comments are supposed to be
meaningful only in the context of the specific function but not when
the same address appears within different functions. However, local
functions are not completely implemented yet.

Within comments, special placeholders exist. This way functions (and,
later, other objects cann be referenced in a way that automatically
updates the displayed string inside the comment. The syntax for a
function reference is ``#F<0xABCD1234>``.

Saving progress
===============

Files saved by frida are standalone. They can be opened and used by
frida on the same or a different computer and do *not* require the
original binary to be available on that machine. Function names as
well as the CFG are preserved, however the textual representation and
the graph layout are reconstructed when opening the saved file.

Hot-keys
========

+---+--------------------------------------------------------+
| r | Rename currently active function                       |
+---+--------------------------------------------------------+
| f | Ask for an address and disassembles the function there |
+---+--------------------------------------------------------+
